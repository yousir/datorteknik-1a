#!/usr/bin/env bash
set -v

HOME="$(dirname "$(realpath "$0")")"

for x in {1..6}; do
	touch -d "2021-01-0$x + $RANDOM seconds" ~/file.blah$x
done

sleep 2


~/../mittskript.sh 2021-01-04 -s blah? --vackert

sleep 2


tail -n+1 ~/blah1 ~/asdf/blah2

sleep 2


diff -s ~/blah1 ~/asdf/blah2

sleep 2


rm -rf ~/blah1 ~/asdf ~/file.blah?
