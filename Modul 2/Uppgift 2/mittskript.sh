#!/usr/bin/env bash

halp() {
    printf '%s\n' """Användning: $0 [-s] FILÄNDELSE DATUM

Lista filer enligt sökkriterier och spara automatiskt resultatet i två
filer ($HOME/blah1 och $HOME/asdf/blah2) samt till /dev/stdout.

      -s, -o, -r, --senast, --omvänd, --reverse
                            Omvänder resultatet baserat på datumet.
                            Har ingen effekt utan DATUM.

      --vackert             Skriver ut tabellrubriker och angivna
                            sökkriterier.

      -h, --help, --hjälp, HAALP
                            Skriver ut denna hjälpmeny.

      FILÄNDELSE            Matcha endast filändelser bestående av
                            bokstäver och/eller siffror med eller utan
                            punkt-prefix.

      DATUM                 Matcha filer som är nyare än angivet datum,
                            eller äldre om använd tillsammans med
                            --senast. DATUM matchas i formatet YYYYMMDD
                            med eller utan någon kombination av
                            skiljetecknen '.', '-' och '/'. YYYY kan
                            också vara i kortform (YY), MM kan vara med
                            eller utan ledande nolla (t.ex. 03) och kan
                            högst vara siffran 12, likadant för DD
                            förutom att högsta möjliga siffran är 31.

 """
    exit ${1:-0}
}

# Om inga argument anges, visa hjälpmenyn
[[ $# -eq 0 ]] && halp

# Aktivera utökad globbing-funktion (för att matcha datum och filändelse)
shopt -s extglob

for arg; do # En förkortning av `for arg in "$@"; do ...`
    case $arg in # En case-sats baserat på värdet av $arg
        -h|--help|--hjälp|HALP) halp ;;
        
        --vackert) vackert=yes ;; # Stiligare utskrift

        -s|-o|-r|--senast|--omvänd|--reverse)
            # argumentet kan vara något av de angivna (separerade med
            # `|`). Omvänder resultatet baserat på datumet. Har ingen
            # effekt utan datum.
            rev=yes ;; # fortsätt case-satsen med hjälp av `;;`

        +([0-9])?([-./])@(?([0])@([0-9])|10|11|12)?([-./])@(?([0-2])@([0-9])|30|31))
                       # använd utökad globbing-funktioner för att
                       # matcha datum i olika format YYYYMMDD med
                       # eller utan någon kombination av skiljetecknen
                       # ".", "-" och "/". YYYY kan vara kortformat
                       # (YY), MM kan vara med eller utan ledande
                       # nolla (t.ex. 03) och kan högst vara siffran
                       # 12, likadant för DD förutom att högsta
                       # möjliga siffran är 31.
            dt=$arg ;;
        ?(.)[A-z0-9]*) # samma här: matcha filändelser bestående av
                       # bokstäver och/eller siffror med eller utan
                       # punkt-prefix.
            ext=$arg ;;
    esac
done

# Skapa mappen "asdf" i $HOME med argumentet `-p` för att inte varna
# om den redan existerar.
mkdir -p ~/asdf

## Lista samtliga filer baserat på angivna parametrar

# 0. Kör kommandot `find` med följande argument:
# 1. Sök i hemma-mappen (~/ == $HOME)
# 2. Sök endast i första mapp-nivån, icke-rekursivt (-maxdepth 1)
# 3. Sök endast efter filer (-type f)

# 4. Ange korrekt urval enligt datum via parameter-expansion. Endast
# om $dt existerar: ange alternativa strängen `!` (endast om $rev
# existerar) samt alternativa strängen `-newermt` följt av
# datumsträngen utan skiljetecken. Utelämna helt om $dt INTE
# existerar. (${dt:+${rev:+!} -newermt ${dt//[^0-9]}})

# 5. Ange korrekt glob med filändelse (t.ex. "*.txt"). Om $ext
# existerar: formatera filändelsen så att den alltid är prefixad med
# en punkt (.) och ange sedan detta som alternativ sträng tillsammans
# med en asterisk (*) som prefix för att bilda en global sökterm som
# t.ex. "*.txt". Om $ext INTE existerar: Utelämna filändelsen helt och
# bilda således endast en asterisk som sökterm för att söka utan
# filnamnskriterier (samma effekt som att inte ange någon sökterm
# ö.h.t). (-iname "*${ext:+.${ext#.}}")

# 6. Omdirigera /dev/stderr (felmeddelanden, t.ex. läsrättigheter
# saknas) till /dev/null, dvs ta bort det helt och hållet.

# Pipe:a sedan resultatet från `find` till kommandot `tee` som skriver
# resultatet till filerna $HOME/blah1 och $HOME/asdf/blah2 samt till
# /dev/stdout (terminalfönstret). Här skulle vi kunna använda `find`:s
# `-exec`-flagga men `tee` är enligt mig mycket enklare och mer robust
# för detta ändamål.

# `\` i slutet av varje argument betyder att argument(et/en) inte är
# slut och fortsätter på raden nedanför. Detta fungerar för
# pipe-tecknet också (|). Används för att dela upp kommandorader och
# på så vis göra det mer läs- och hanterbart än om allt skulle vara på
# en enda lång rad.

rubriker=(
    "${dt:+Datum: $([[ $rev ]] && printf senast || printf tidigast) $dt\n\n}"
    "${ext:+${dt:+$(tput cuu1;tput el)}Filändelse: .${ext#.}\n\n}"
    Datum
    Klockslag
    Fil
)

{ ${vackert:+printf "%b%b%-12s %-21s %s\n" "${rubriker[@]}"} 
  find ~/ \
       -maxdepth 1 \
       -type f \
       ${dt:+${rev:+!} -newermt ${dt//[^0-9]}} \
       -iname "*${ext:+.${ext#.}}" \
       -printf "%Tx ${vackert:+|} %TT ${vackert:+|} %p\n" \
       2>/dev/null ;} \
    | tee ~/blah1 ~/asdf/blah2
